# ublock-browser-real-estate

uBlock filter that removes elements that take too much browser real estate.

In uBlock Origin, go to the tab Filter Lists, and at the bottom click on 
"Import..." and paste this:

```
https://gitlab.com/gasull/ublock-browser-real-estate/raw/master/rules.txt
```